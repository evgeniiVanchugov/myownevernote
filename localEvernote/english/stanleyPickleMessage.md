##Message of Stanley Pickle

A short movie Stanley Pickle woke some controversial feelings inside of me. At one hand, that was a really creepy and
disturbing experience. As I mentioned in classes, people with fear of mannequin may become mad while watching this.
But on the other hand, such filming technique, so-called "**stop-motion**", require a lot of patience and skill.
As a result, it looks fantastic, fascinating and charming!  

But what about message of this peace of cinematic mastery? While watching this film in classes, two similar points was
born.

The first one sounds like this: parents of Stanley has passed away a long time ago. He very loved them while they
were alive and his love was not ended after their death. So he decided to bring them back to live with his machinery
skills. And he was succeed at it! His actions represents not really excavation of dead bodies of parents and making
them mechanized zombies (Brrrr 0~~~0), but represents his attempts to stay in that time, when they all was together.
He feels fear to let go those memories and move forward with ease. And his decision to left his memories was showed as
burying his "parents".

The second point was not quite popular, but in my opinion it was the main message of the film. The idea is simple:
Stanly - growth child, who live with parents and afraid leaving warm parent's nest. Why this point? Why not other?
Because of three things:

1. He doesn't looks like a child, but lives in his old child's room and playing with kids toys;
2. Again, he doesn't looks like a child at all, but he sleeps in the bed and his legs stick out of the bounds;
3. That episode, when he fixes his parents when they broke, shows us the fact, that at some age teenagers become.

smarter then parents, while parents become more frigid and fragile.

And episode, when he bury his parents in the end of the movie, shows us that he finally decide to move out from parent's
warm house.